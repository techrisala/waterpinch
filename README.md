## WaterPinch

A MATLAB based toolbox which facilitates single contaminant based water pinch analysis. The applications of this toolbox lies in both academia and industries wherein it can aid its users in developing a basic understanding of targeting and network design procedures, and generating practical water network solutions for real life processes.

## Status Log
* Part of Undergraduate Thesis/Project for BE Petrochemical Engineering.
* Ideas to further extend the toolbox can be found in the report (especially, in concluding remarks) present in this repository.
